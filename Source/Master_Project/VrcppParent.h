// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "VrcppParent.generated.h"

UCLASS()
class MASTER_PROJECT_API AVrcppParent : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVrcppParent();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	
	UFUNCTION(BlueprintCallable, Category = "VR_Greger")
	void startVoiceCP();
	
	
};

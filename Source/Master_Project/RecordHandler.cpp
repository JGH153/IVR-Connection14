// Fill out your copyright notice in the Description page of Project Settings.

#include "Master_Project.h"
#include "RecordHandler.h"


// Sets default values
ARecordHandler::ARecordHandler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARecordHandler::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("YOLO Your message 123 456 789"));
	//not working due to:
	//LogNet: CreateNamedNetDriver failed to create driver from definition DemoNetDriver
	UGameplayStatics::GetGameInstance(GetWorld())->StartRecordingReplay(TEXT(""), TEXT("My Replay"));
	this->isRecording = true;
}

// Called every frame
void ARecordHandler::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (this->isRecording) {
		this->cumulatedTime += DeltaTime;
		
		if (this->cumulatedTime > 3.f) {
			this->isRecording = false;
			UE_LOG(LogTemp, Warning, TEXT("YOLO rec done 1337"));
			UGameplayStatics::GetGameInstance(GetWorld())->StopRecordingReplay();
		}

	}

	

}

